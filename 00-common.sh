#!/usr/bin/env bash

# Commonly used aliases
if [[ "$(uname -s)" == "Darwin" ]]; then
    # OS is Mac OS
    alias ls='ls -FG'
    export CLICOLOR=1
    export LSCOLORS=ExFxCxDxBxegedabagacad
else
    # OS is Linux
    alias ls='ls -F --color=auto'
    alias ip='ip -c'
    alias diff='diff --color=auto'
    # Remove squashfs (snaps) from df
    alias df='df -x"squashfs"'
fi

# fd - cd to selected directory
fd() {
  local dir
  dir=$(find "${1:-.}" -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir" || return
}

alias grep='grep --color=auto'
alias l='ls -F'
alias ll='ls -l'
alias la='ls -las'
alias duh='du -hsx * 2>/dev/null | sort -hr | head -n 6'

