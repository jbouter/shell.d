#!/usr/bin/env bash


alias k='kubectl'

function kns(){
    if [ -n "$1" ]; then
      kubectl config set-context --current --namespace="$1"
    fi
}

function kct(){
    if [ -n "$1" ]; then
      kubectl config use-context "$1"
    fi
}

function kseal() {
  echo -n "$1" | kubeseal --raw --from-file=/dev/stdin -o yaml --namespace "$2" --controller-name sealed-secrets --controller-namespace=sealed-secrets --name "$3"
}

