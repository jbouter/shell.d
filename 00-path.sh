#!/usr/bin/env bash

# Update PATH variable
export PATH=$PATH:$HOME/.local/bin:/usr/local/sbin:/usr/sbin:/sbin
