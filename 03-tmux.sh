#!/usr/bin/env bash

# custom function to start a new SSH session in a new tmux window
function t () {
  if tmux list-windows | grep -qE "[0-9]: $1" >/dev/null 2>&1; then
    tmux select-window -t "$1"
  else
    tmux new-window -n "$1" "ssh $1"
  fi
}


function ssh-multi() {
  tmux new-window -n "ssh-multi" "ssh $1"
  for i in "${@:2}"; do
    tmux split-window -h "ssh $i"
    tmux select-layout tiled > /dev/null
  done
  tmux select-pane -t 0
  tmux set-window-option synchronize-panes on > /dev/null
}
