#!/usr/bin/env bash

# Virsh
alias virsh='virsh -c qemu:///system'
alias virt-viewer='virt-viewer -c qemu:///system'
alias virt-install='virt-install --connect qemu:///system'
