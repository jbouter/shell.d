# shell config directory

Setting up these aliases in two steps. First, clone the repository to `~/.config/shell.d`:

```bash
git clone https://gitlab.com/jbouter/shell.d.git
```

Then, add the following to the bottom of your `~/.zshrc` or `~/.bashrc`:

```bash
# Load custom zsh config
for config in ~/.config/shell.d/*.sh; do
  source "$config"
done
```
